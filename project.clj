(defproject de.danjou/clj-mdc "0.1.4"
  :description "Utils for dealing with MDC"
  :url "https://gitlab.com/dAnjou/clj-mdc"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url  "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.slf4j/slf4j-api "1.7.29"]
                 [org.clojure/tools.logging "0.5.0" :exclusions [org.clojure/clojure]]]
  :repl-options {:init-ns clj-mdc.core}
  :profiles {:common {:dependencies [[org.clojure/clojure "1.10.1"]
                                     [ch.qos.logback/logback-classic "1.2.3"]]}
             :dev    [:common]
             :test   [:common]}
  :plugins [[lein-project-version "0.1.0"]
            [lein-ancient "0.6.15"]]
  :scm {:name "git" :url "https://gitlab.com/dAnjou/clj-mdc"}
  :deploy-repositories [["clojars" {:url           "https://clojars.org/repo"
                                    :username      :env/clojars_username
                                    :password      :env/clojars_password
                                    :sign-releases false}]])
