# clj-mdc

Utils for dealing with MDC

[![Clojars Project](https://img.shields.io/clojars/v/de.danjou/clj-mdc.svg)](https://clojars.org/de.danjou/clj-mdc)

## Features

- `with-mdc` - macro for adding values to the MDC
